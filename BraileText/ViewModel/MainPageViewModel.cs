﻿using BraileText.Model;
using BraileText.Services;

using BrailleToTextTransformer.Models;
using BrailleToTextTransformer.Services;

using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;

using Microsoft.Win32;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace BraileText.ViewModel
{
    class MainPageViewModel : ViewModelBase
    {
        private Dictionary<string, Action<string>> FileFormatReadActionDictionary => new Dictionary<string, Action<string>>
        {
            { ".txt", ReadDocumentToInputBox<TxtDocument>},
            { ".docx", ReadDocumentToInputBox<DocxDocument>},
            { ".doc", ReadDocumentToInputBox<DocxDocument>},
            { ".pdf", ReadDocumentToInputBox<PDFDocument>},
            { ".png", ReadDocumentToInputBox<ImageDocument>},
            { ".jpeg", ReadDocumentToInputBox<ImageDocument>},
            { ".jpg", ReadDocumentToInputBox<ImageDocument>}
        };
        private Dictionary<string, Action<string>> FileFormatWriteActionDictionary => new Dictionary<string, Action<string>>
        {
            { ".txt", WriteDocumentToInputBox<TxtDocument>},
            { ".docx", WriteDocumentToInputBox<DocxDocument>},
            { ".doc", WriteDocumentToInputBox<DocxDocument>},
            { ".pdf", WriteDocumentToInputBox<PDFDocument>},
            { ".png", WriteDocumentToInputBox<ImageDocument>},
            { ".jpeg", WriteDocumentToInputBox<ImageDocument>},
            { ".jpg", WriteDocumentToInputBox<ImageDocument>}
        };

        private string inputString = "";
        private string outputString = "";
        private bool isReverseTranslateEnabled;
        private bool isEnglishEnabled = false;
        private bool isRussianEnabled = true;
        public Language language = Language.Russian;

        public bool IsEnglishEnabled
        {
            get => isEnglishEnabled;
            set 
            { 
                isEnglishEnabled = value;
                if (value == true)
                    SwitchLanguage(Language.English);
                RaisePropertyChanged();
                return;
            }
        }
        public bool IsRussianEnabled
        {
            get => isRussianEnabled;
            set 
            { 
                isRussianEnabled = value;
                if(value == true)
                    SwitchLanguage(Language.Russian);
                RaisePropertyChanged();
            }
        }
        public string InputString
        {
            get => inputString;
            set
            {
                inputString = value;
                AutoTranslate();
                RaisePropertyChanged();
            }
        }

        public string OutputString
        {
            get => outputString;
            set
            {
                outputString = value;
                RaisePropertyChanged();
            }
        }

        public bool IsReverseTranslateEnabled
        {
            get => isReverseTranslateEnabled;
            set 
            { 
                isReverseTranslateEnabled = value; 
                RaisePropertyChanged(); 
            }
        }
        
        [Command]
        public void BrowseCommand()
        {
            var browseFileDialog = GetFileDialog<OpenFileDialog>();            
            if ((bool)browseFileDialog.ShowDialog())
                ReadSelectedFileIntoInputBox(browseFileDialog.FileName);
        }

        [Command]
        public void CleanUpCommand()
        {
            InputString = "";
        }

        [Command]
        public void SaveCommand()
        {
            var saveFileDialog = GetFileDialog<SaveFileDialog>();
            if ((bool)saveFileDialog.ShowDialog())
                WriteOutputToSelectedFile(saveFileDialog.FileName);
        }

        [Command]
        public void InputBraileSymbolCommand(string parameter)
        {
            if (!IsReverseTranslateEnabled) IsReverseTranslateEnabled = true;
            LanguageChecking();
            InputString += parameter;
        }

        private void AutoTranslate()
        {
            Task.Factory.StartNew(() =>
            {
                LanguageChecking();
                var translator = new TextTranslator(language, IsReverseTranslateEnabled);
                OutputString = translator.Translate(InputString);
            });
        }
        private void LanguageChecking()
        {
            TryToDetectLanguage(InputString);
            if (IsRussianEnabled) SwitchLanguage(Language.Russian);
            else if (IsEnglishEnabled) SwitchLanguage(Language.English);
            else throw new Exception("There is some strange problems in Language");
        }

        private void ReadSelectedFileIntoInputBox(string pathToFile)
        {
            string extension = Path.GetExtension(pathToFile);

            if (!FileFormatReadActionDictionary.ContainsKey(extension))
                MessageBox.Show($"This file format {extension} doesn't support now. Please, use doc/docx/pdf/txt!");

            FileFormatReadActionDictionary[extension](pathToFile);
        }
        private void WriteOutputToSelectedFile(string pathToFile)
        {
            string extension = Path.GetExtension(pathToFile);
            
            if (!FileFormatWriteActionDictionary.ContainsKey(extension))
                MessageBox.Show($"This file format {extension} doesn't support now. Please, use doc/docx/pdf/txt!");
            
            FileFormatWriteActionDictionary[extension](pathToFile);
        }
        private void ReadDocumentToInputBox<T>(string browseFileName) where T: DocumentBase, new()
        {
            DocumentBase doc;
            if (typeof(T) != typeof(ImageDocument))
               doc = (T)Activator.CreateInstance(typeof(T), browseFileName);
            else doc = (T)Activator.CreateInstance(typeof(T), browseFileName, language);

            Task.Factory.StartNew(() => InputString = doc.ReadAllLines().Result);
        }

        private void WriteDocumentToInputBox<T>(string browseFileName) where T: DocumentBase, new()
        {
            DocumentBase doc = (T)Activator.CreateInstance(typeof(T), browseFileName);
            Task.Factory.StartNew(() => doc.WriteAllLines($"{InputString}\n {OutputString}"));
        }

        private void SwitchLanguage(Language value)
        {
            language = value;
        }

        private T GetFileDialog<T>() where T: FileDialog, new()
        {
            var filter = "Text Files (*.txt)|*.txt|Documents (*.doc; *.docx)|*.rtf; *.docx|Image files (*.jpg; *.jpeg; *.png)|*.jpg; *.jpeg; *.png|PDF (*.pdf)|*.pdf|All files (*.*)|*.*";
            return new T { Filter = filter };
        }

        private void TryToDetectLanguage(string value)
        {
            var firstChar = value.FirstOrDefault();
            if (firstChar == default) return;

            if (IsRussian(value) && !IsReverseTranslateEnabled)
            {
                IsRussianEnabled = true;
                IsEnglishEnabled = false;
                IsReverseTranslateEnabled = false;
                return;
            }
            else if (IsEnglish(value) && !IsReverseTranslateEnabled)
            {
                IsRussianEnabled = false;
                IsEnglishEnabled = true;
                IsReverseTranslateEnabled = false;
                return;
            }
            else if (value.Any(item => item >= '0' && item <= '9')) return;
            else IsReverseTranslateEnabled = true;
        }

        private bool IsRussian(string value) => value.Any(item => (item >= 'А' && item <= 'Я') || (item >= 'а' && item <= 'я'));
        private bool IsEnglish(string value) => value.Any(item => (item >= 'A' && item <= 'Z') || (item >= 'a' && item <= 'z'));
    }
}
