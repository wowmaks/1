﻿using System.IO;
using System.Threading.Tasks;

namespace BraileText.Model
{
    public abstract class DocumentBase
    {
        protected string PathToFile { get; }

        /// <summary>
        /// Don't use it for object init. It used for T
        /// </summary>
        public DocumentBase()
        {

        }

        public DocumentBase(string pathToFile)
        {
            PathToFile = pathToFile;
        }

        public virtual Task<string> ReadAllLines()
        {
            if (!File.Exists(PathToFile)) throw new FileNotFoundException($"File not found. Path {PathToFile}");

            return Task.FromResult("");
        }

        public abstract Task WriteAllLines(string text);
    }
}
