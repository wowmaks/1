﻿using BraileText.Model;

using BrailleToTextTransformer.Models;

using IronOcr;
using IronOcr.Languages;

using System;
using System.Drawing;
using System.Threading.Tasks;

namespace BraileText.Services
{
    public sealed class ImageDocument : DocumentBase
    {
        public IOcrLanguagePack LanguagePack { get; }

        public ImageDocument() : base()
        {

        }

        public ImageDocument(string pathToFile) : base(pathToFile)
        {

        }

        public ImageDocument(string pathToFile, Language language) : this(pathToFile)
        {
            if (language == Language.English)
                LanguagePack = English.OcrLanguagePack;
            else if (language == Language.Russian) LanguagePack = Russian.OcrLanguagePack;
        }

        public override Task WriteAllLines(string text)
        {
            DrawText(text).Save(PathToFile);

            return Task.CompletedTask;
        }

        public override Task<string> ReadAllLines()
        {
            base.ReadAllLines();

            return Task.FromResult(ConvertertImageToText());
        }

        private string ConvertertImageToText()
        {
            var ocr = new AdvancedOcr() { Language = LanguagePack };
            return ocr.Read(PathToFile).Text;
        }

        /// <summary>
        /// Creates an image containing the given text.
        /// NOTE: the image should be disposed after use.
        /// </summary>
        /// <param name="text">Text to draw</param>
        /// <param name="fontOptional">Font to use, defaults to Control.DefaultFont</param>
        /// <param name="textColorOptional">Text color, defaults to Black</param>
        /// <param name="backColorOptional">Background color, defaults to white</param>
        /// <param name="minSizeOptional">Minimum image size, defaults the size required to display the text</param>
        /// <returns>The image containing the text, which should be disposed after use</returns>
        private Image DrawText(string text, Font fontOptional = null, Color? textColorOptional = null, Color? backColorOptional = null, Size? minSizeOptional = null)
        {
            Font font = SetOptionalOrDefaultFont(fontOptional);
            Color textColor = SetOptionalOrDefaultTextColor(textColorOptional);
            Color backColor = SetOptionalOrDefaultBackgroundColor(backColorOptional);
            Size minSize = SetOptionalOrDefaultMinSize(minSizeOptional);

            SizeF textSize = ComputeTextSizeForImage(text, font, minSize);
            
            return CreateImage(text, font, textColor, backColor, textSize);
        }

        private static Size SetOptionalOrDefaultMinSize(Size? minSizeOptional)
        {
            Size minSize = Size.Empty;
            if (minSizeOptional != null)
                minSize = (Size)minSizeOptional;
            return minSize;
        }

        private static Color SetOptionalOrDefaultBackgroundColor(Color? backColorOptional)
        {
            Color backColor = Color.White;
            if (backColorOptional != null)
                backColor = (Color)backColorOptional;
            return backColor;
        }

        private static Color SetOptionalOrDefaultTextColor(Color? textColorOptional)
        {
            Color textColor = Color.Black;
            if (textColorOptional != null)
                textColor = (Color)textColorOptional;
            return textColor;
        }

        private static Font SetOptionalOrDefaultFont(Font fontOptional)
        {
            Font font = new Font("Times New Roman", 10, FontStyle.Regular); ;
            if (fontOptional != null)
                font = fontOptional;
            return font;
        }

        private static Image CreateImage(string text, Font font, Color textColor, Color backColor, SizeF textSize)
        {

            //create a new image of the right size
            Image retImg = new Bitmap((int)textSize.Width, (int)textSize.Height);
            using (var drawing = Graphics.FromImage(retImg))
            {
                //paint the background
                drawing.Clear(backColor);

                //create a brush for the text
                using (Brush textBrush = new SolidBrush(textColor))
                {
                    drawing.DrawString(text, font, textBrush, 0, 0);
                    drawing.Save();
                }
            }

            return retImg;
        }

        private static SizeF ComputeTextSizeForImage(string text, Font font, Size minSize)
        {
            SizeF textSize;
            using (Image img = new Bitmap(1, 1))
            {
                using (Graphics drawing = Graphics.FromImage(img))
                {
                    //measure the string to see how big the image needs to be
                    textSize = drawing.MeasureString(text, font);
                    if (!minSize.IsEmpty)
                    {
                        textSize.Width = textSize.Width > minSize.Width ? textSize.Width : minSize.Width;
                        textSize.Height = textSize.Height > minSize.Height ? textSize.Height : minSize.Height;
                    }
                }
            }
            return textSize;
        }

        [Flags]
        private enum TesseractLanguages
        {
            eng = 1,
            rus = 1 << 1
        }
    }
}
