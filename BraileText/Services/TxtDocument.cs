﻿using BraileText.Model;

using System.IO;
using System.Threading.Tasks;

namespace BraileText.Services
{
    public sealed class TxtDocument : DocumentBase
    {
        public TxtDocument() : base() { }
        public TxtDocument(string pathToFile) : base(pathToFile) { }
        public override Task<string> ReadAllLines()
        {
            base.ReadAllLines();
            return Task.FromResult(File.ReadAllText(PathToFile));
        }

        public override Task WriteAllLines(string text)
        {
            File.WriteAllText(PathToFile, text);
            return Task.CompletedTask;
        }
    }
}
