﻿using BraileText.Model;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

using System.IO;
using System.Threading.Tasks;

namespace BraileText.Services
{
    public sealed class DocxDocument : DocumentBase
    {
        public DocxDocument() : base() { }
        public DocxDocument(string pathToFile) : base(pathToFile) { }

        public override Task<string> ReadAllLines()
        {
            base.ReadAllLines();
            return Task.FromResult(ReadTextFromDocument());
        }

        public override Task WriteAllLines(string text)
        {
            using (var wordDocument = WordprocessingDocument.Create(PathToFile, WordprocessingDocumentType.Document))
            {
                MainDocumentPart mainPart = wordDocument.AddMainDocumentPart();

                mainPart.Document = new Document();
                Body body = mainPart.Document.AppendChild(new Body());
                Paragraph para = body.AppendChild(new Paragraph());
                Run run = para.AppendChild(new Run());
                run.AppendChild(new Text(text));
            }
            return Task.CompletedTask;
        }

        private string ReadTextFromDocument()
        {
            try
            {
                using (var document = WordprocessingDocument.Open(PathToFile, true))
                {
                    return document.MainDocumentPart.Document.Body.InnerText;
                }
            }
            catch (FileFormatException ex)
            {
                throw ex;
            }
            catch (IOException ex)
            {
                throw ex;
            }
        }
 
    }
}
