﻿using BraileText.Extensions;
using BraileText.Model;

using System;
using System.Linq;

namespace BraileText.Services.Translators
{
    public sealed class TextTranslator : ITranslator
    {
        private MultilangualTranslator LanguageTranslator { get; }
        private NumericTranslator NumericTranslator { get; }
        private SpecialTranslator SpecialSymbolTranslator { get; }
        private bool IsReverseTranslation { get; }

        public TextTranslator(Language language, bool isReverseTranslation = false)
        {
            LanguageTranslator = new MultilangualTranslator(language, isReverseTranslation);
            NumericTranslator = new NumericTranslator(isReverseTranslation);
            SpecialSymbolTranslator = new SpecialTranslator(isReverseTranslation);
            IsReverseTranslation = isReverseTranslation;
        }

        public string Translate(string input) => ConvertTextToBraile(input);

        public string TranslateChar(char input) => ConvertTextToBraile(input.ToString());

        private string ConvertTextToBraile(string input)
        {
            if (string.IsNullOrEmpty(input)) return "";

            var result = "";
            var previosSkipAppend = 0;
            var internalSkip = 0;
            while (!IsAllTextWasTranslated(input, internalSkip))
            {
                var forLangTranslation = GetPartOfString(input, LanguageTranslator.CanTranslate, ref internalSkip);

                var forSpecialTranslation = GetPartOfString(input, SpecialSymbolTranslator.CanTranslate, ref internalSkip);

                var forNumericTranslation = GetPartOfString(
                    input,
                    NumericTranslator.CanTranslate,
                    ref internalSkip,
                    partOfString => partOfString.ValidateIsNumericSubstring(IsReverseTranslation));

                result += $"{LanguageTranslator.Translate(forLangTranslation)}" +
                          $"{SpecialSymbolTranslator.Translate(forSpecialTranslation)}" +
                          $"{NumericTranslator.Translate(forNumericTranslation)}";

                if (IsUntranslatedSymbolWasMeeted(previosSkipAppend, internalSkip))
                    result += GetUntranslatedSymbol(input, ref internalSkip);

                previosSkipAppend = internalSkip;
            }
            return result;
        }

        private static bool IsAllTextWasTranslated(string input, int internalSkip) => internalSkip >= input.Length;

        private static string GetUntranslatedSymbol(string input, ref int internalSkip)
        {
            var unknownSymbol = string.Join("", input.Skip(internalSkip).Take(1));
            internalSkip++;
            return unknownSymbol;
        }

        private static bool IsUntranslatedSymbolWasMeeted(int previosSkipAppend, int internalSkip) => internalSkip - previosSkipAppend == 0;

        private static string GetPartOfString(string input, Func<char, bool> takeWhile, ref int valueForSkip, Func<string, string> validateResult = null)
        {
            var forTranslation = string.Join("", input.Skip(valueForSkip).TakeWhile(takeWhile));

            forTranslation = validateResult == null ? forTranslation : validateResult(forTranslation);

            valueForSkip += forTranslation.Count();

            return forTranslation;
        }
    }
}
