﻿using BraileText.Model;

using System.Collections.Generic;
using System.Linq;

namespace BraileText.Services.Translators
{
    public sealed class SpecialTranslator : TranslatorBase
    {
        public SpecialTranslator(bool isReverseTranslation = false) : base(isReverseTranslation)
            => TranslatorDictionary = CreateTranslationDictionary(isReverseTranslation);

        public override string Translate(string input) => base.Translate(input);

        public override string TranslateChar(char input) => GetTranslatedChar(input).ToString();

        private char GetTranslatedChar(char input) => TranslatorDictionary[input.ToString()].First();

        private Dictionary<string, string> CreateTranslationDictionary(bool isReverseTranslation)
        {
            var result = new Dictionary<string, string>
            {
                { ".", "⠲" },
                { ",", "⠂" },
                { "!", "⠖" },
                { "?", "⠢" },
                { "-", "⠤" },
                { " ", " " },
            };

            return isReverseTranslation
                        ? result.ToDictionary(originalDict => originalDict.Value, originalDict => originalDict.Key)
                        : result;
        }
    }
}
