﻿using BraileText.Model;

using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Kernel.Pdf.Canvas.Parser.Listener;
using iText.Layout;
using iText.Layout.Element;

using System.Threading.Tasks;

namespace BraileText.Services
{
    public sealed class PDFDocument : DocumentBase
    {
        public PDFDocument() : base() { }
        public PDFDocument(string pathToFile) : base(pathToFile) { }

        public override Task<string> ReadAllLines()
        {
            base.ReadAllLines();
            return Task.FromResult(ReadAllTextFromPDF(PathToFile));
        }
        public override Task WriteAllLines(string text)
        {
            using (var textWriter = new PdfWriter(PathToFile))
            {
                var pdfDoc = new PdfDocument(textWriter);
                var doc = new Document(pdfDoc);
                var textParagraph = new Paragraph(text);

                pdfDoc.AddNewPage();
                doc.SetFontSize(14);
                doc.Add(textParagraph);

                pdfDoc.Close();
                doc.Close();
            }
            return Task.CompletedTask;
        }

        private static string ReadAllTextFromPDF(string filePath) 
        {
            var result = "";
            PdfReader pdfReader = new PdfReader(filePath); 
            var pdfDoc = new PdfDocument(pdfReader); 
            for (int page = 1; page <= pdfDoc.GetNumberOfPages(); page++) 
            { 
                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy(); 
                result += PdfTextExtractor.GetTextFromPage(pdfDoc.GetPage(page), strategy); 
            } 
            pdfDoc.Close(); 
            pdfReader.Close();

            return result;
        }

    }
}
