﻿using BraileText.Model;
using BraileText.Services;
using BraileText.Services.Translators;

using Xunit;

namespace BraileTest
{
    public sealed class ReverseTranslatorTest
    {
        [Fact]
        public void ReverseNumericTranslatorTest()
        {
            var result = new NumericTranslator(isReverseTranslation: true).Translate("⠁");

            Assert.Equal("1", result);

            result = new NumericTranslator(isReverseTranslation: true).Translate("⠼⠁");

            Assert.Equal("1", result);
        }

        [Fact]
        public void ReverseRussianTranslatorTest()
        {
            var resultRussian = new MultilangualTranslator(Language.Russian, isReverseTranslation: true).Translate("⠠⠏⠗⠊⠺⠑⠞");

            Assert.Equal("Привет", resultRussian);
        }
        [Fact]
        public void ReverseEnglishTranslatorTest()
        {
            var resultEnglish = new MultilangualTranslator(Language.English, isReverseTranslation: true).Translate("⠠⠓⠑⠇⠇⠕");

            Assert.Equal("Hello", resultEnglish);
        }

        [Fact]
        public void ReverseTextTranslatorTest()
        {
            var resultRussian = new TextTranslator(Language.Russian, isReverseTranslation: true).Translate("⠠⠏⠗⠊⠺⠑⠞ ⠍⠊⠗⠲ ⠠⠝⠕ ⠼⠁⠃⠉ ⠼⠁⠃⠉");

            Assert.Equal("Привет мир. Но 123 123", resultRussian);

            var resultEnglish = new TextTranslator(Language.English, isReverseTranslation: true).Translate("⠠⠓⠑⠇⠇⠕ ⠍⠽ ⠙⠑⠁⠗ ⠙⠥⠙⠑⠲ ⠼⠁⠃⠉ ⠁⠏⠏⠇⠑⠼⠁⠃⠉");

            Assert.Equal("Hello my dear dude. 123 apple123", resultEnglish);
        }


    }
}
