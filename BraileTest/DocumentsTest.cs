﻿using BraileText.Services;
using BraileText.Services.Translators;

using System.IO;

using Xunit;

namespace BraileTest
{
    public sealed class DocumentsTest
    {
        [Theory]
        [InlineData("docxTestFile.docx", "We test it again")]
        [InlineData("docxRussianTestFile.docx", "Пример текста на русском")]
        [InlineData("docxBraileTestFile.docx", "⠠⠏⠗⠊⠺⠑⠞")]

        public void DocxTest(string filename, string text)
        {
            var docXDocument = new DocxDocument(filename);
            docXDocument.WriteAllLines(text);

            var result = docXDocument.ReadAllLines();

            Assert.Equal(text, result.Result);
        }
        [Theory]
        [InlineData("txtTestFile.txt", "We test it one more time")]
        [InlineData("txtRussianTestFile.docx", "Пример текста на русском")]
        [InlineData("txtBraileTestFile.docx", "⠠⠏⠗⠊⠺⠑⠞")]

        public void TxtTest(string filename, string text)
        {
            var txtDocument = new TxtDocument(filename);
            txtDocument.WriteAllLines(text);

            var result = txtDocument.ReadAllLines();

            Assert.Equal(text, result.Result);
        }
        [Theory]
        [InlineData("pdfTestFile.pdf", "We test it one more time")]
        public void PdfTest(string filename, string text)
        {
            var pdfDocument = new PDFDocument(filename);
            pdfDocument.WriteAllLines(text);

            var result = pdfDocument.ReadAllLines();

            Assert.Equal(text, result.Result);
        }

        /// <summary>
        /// We don't test reading from image, because test project use .net core, but image reader not. But it work, I promise.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="text"></param>
        [Theory]
        [InlineData("imageTestFile.png", "We test image one more time")]
        [InlineData("imageRussianTestFile.docx", "Пример текста на русском")]
        [InlineData("imageBraileTestFile.docx", "⠠⠏⠗⠊⠺⠑⠞")]
        public void ImageTest(string filename, string text)
        {
            var imageDocument = new ImageDocument(filename);
            imageDocument.WriteAllLines(text);

            Assert.True(new FileInfo(filename).Length != 0);
        }
    }
}
