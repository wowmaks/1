﻿using BraileText.Model;
using BraileText.Services;
using BraileText.Services.Translators;

using Xunit;

namespace BraileTest
{
    public sealed class TranslatorsTest
    {
        [Fact]
        public void NumericTranslatorTest()
        {
            var result = new NumericTranslator().Translate("123");

            Assert.Equal("⠼⠁⠃⠉", result);

        }

        [Fact]
        public void RussianTranslatorTest()
        {
            var tChar = new MultilangualTranslator(Language.Russian).Translate("П");

            Assert.Equal("⠠⠏", tChar);

            var russianText = new MultilangualTranslator(Language.Russian).Translate("Привет");

            Assert.Equal("⠠⠏⠗⠊⠺⠑⠞", russianText);
        }

        [Fact]
        public void EnglishTranslatorTest()
        {
            var englishText = new MultilangualTranslator(Language.English).Translate("Hello");

            Assert.Equal("⠠⠓⠑⠇⠇⠕", englishText);
        }

        [Fact]
        public void TextTranslator()
        {
            var russianText = new TextTranslator(Language.Russian).Translate("Привет мир. Но 123 123");

            Assert.Equal("⠠⠏⠗⠊⠺⠑⠞ ⠍⠊⠗⠲ ⠠⠝⠕ ⠼⠁⠃⠉ ⠼⠁⠃⠉", russianText);

            var englishText = new TextTranslator(Language.English).Translate("Hello my dear dude. 123 apple123");

            Assert.Equal("⠠⠓⠑⠇⠇⠕ ⠍⠽ ⠙⠑⠁⠗ ⠙⠥⠙⠑⠲ ⠼⠁⠃⠉ ⠁⠏⠏⠇⠑⠼⠁⠃⠉", englishText);

        }
    }
}
